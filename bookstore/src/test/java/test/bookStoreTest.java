package test;

import bookstore.MembershipType.*;
import bookstore.ExternalClasses.*;
import bookstore.Application.*;

import static org.junit.Assert.assertEquals;
import java.beans.Transient;
import org.junit.Test;

public class bookStoreTest {
    /**
     * Tests Methods within the membership subclasses and class
     */
    @Test
    public void testMembMethods() {
        Membership bronzeMemb = new Bronze();

        //calculateDiscount method
        //Test Values will be a current price of 100 and the bronze discount of 5% off
        assertEquals(95,bronzeMemb.calculateDiscount(100, 0.05) , 0);

        //addToPointsBasedOnMembership method
        assertEquals(0,bronzeMemb.addToPointsBasedOnMembership(3),0);
        assertEquals(10,bronzeMemb.addToPointsBasedOnMembership(10),0);
    }

    /**
     * Tests checkIfEnoughMoney method which is in the payment class
     */
    @Test
    public void testPaymentMethod() {
        //checkIfEnoughMoney Method
        Payment testPayment = new Payment(500,1,"1111111111111111");
        assertEquals(false, testPayment.checkIfEnoughMoney(1000));
        assertEquals(true, testPayment.checkIfEnoughMoney(200));
    }
}
