package bookstore.Javafx;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;

import bookstore.Application.BookStore;
import bookstore.ExternalClasses.Customer;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.paint.*;
import javafx.scene.text.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;
import javafx.stage.Window;

import bookstore.Application.*;
import bookstore.ExternalClasses.*;
import bookstore.Item.Accessories;
import bookstore.Item.Book;
import bookstore.Item.FileItemImporter;
import bookstore.Item.Item;
/**
 * JavaFX App
 */
public class App extends Application {

    Stage primaryStage;
    Scene login,options,account,booksAvailable,preStore,payScreen;
    static boolean isDatabase = false;

    static Cart cart;
    static String booksInCart = "";
    static String accsInCart = "";
    static String customerIDDB;
       
    /* (non-Javadoc)
    Creates javafx, all its scenes and implements functions
     * @see javafx.application.Application#start(javafx.stage.Stage)
     */
    public void start(Stage stage) throws SQLException {
        
        primaryStage = stage;

        //GROUPS
        Group rootType = new Group();
        Group root = new Group();
        Group root2 = new Group();
        Group root3 = new Group();
        Group root4 = new Group();
        Group payment = new Group();

        //CSV or DATABASE
        preStore = new Scene (rootType, 600, 500);
        login = new Scene(root, 600, 500);
        options = new Scene(root2, 600, 500);
        account = new Scene(root3, 600, 500);
        booksAvailable =  new Scene(root4, 1250, 600);
        payScreen = new Scene(payment, 600, 500);

        Font itemsFont = Font.font("Verdana", FontWeight.BOLD, 12);

        HBox optionsButtons = new HBox(15);
        preStore.setFill(Color.BEIGE);
        Button csv = new Button("CSV");
        Button db = new Button("Database");
        optionsButtons.getChildren().addAll(csv,db);

        db.setOnAction(go -> {
            primaryStage.setScene(login);
            isDatabase = true;
        });

        rootType.getChildren().add(optionsButtons);

        csv.setOnAction(e -> primaryStage.setScene(login));

        //LOGIN Page
        login.setFill(Color.LIGHTCORAL);
        options.setFill(Color.SKYBLUE);
        account.setFill(Color.LIGHTGREEN);
        booksAvailable.setFill(Color.LIGHTPINK);
        payScreen.setFill(Color.AZURE);

        stage.setTitle("Bookstore Montreal");
        stage.setScene(preStore);

        Font font = Font.font("Verdana", FontWeight.BOLD, FontPosture.ITALIC, 20);

        Label items = new Label("Items Available");
        items.setFont(font);

        Label welcome = new Label("Welcome to the Montreal Bookstore!");
        welcome.setFont(font);

        HBox nameInfo = new HBox(15);
        Label username = new Label("Customer ID:");
        TextField user = new TextField("");
        nameInfo.getChildren().addAll(username,user);
        Button backToOptions = new Button("Go back to pre store");
        backToOptions.setOnAction(e-> {
            primaryStage.setScene(preStore);
            isDatabase = false;
        });

        VBox customerInfo = new VBox(20);
       
        //button to go to scene 2
        Button loginB = new Button("Login");
        customerInfo.getChildren().addAll(welcome,nameInfo,loginB,backToOptions);

        root.getChildren().addAll(customerInfo);

        loginB.setOnAction(e -> primaryStage.setScene(options));

        //options page and buttons
        Label optionsMessage = new Label("Choose an option");
        optionsMessage.setFont(font);

        VBox buttons = new VBox(20);
        Button info = new Button("Check account status");
        Button goShopping = new Button("Go shopping");
        Button quit = new Button("Quit Program");

        VBox menuInfo = new VBox(15);
        Button quit2 = new Button("Go back to menu");
        Label description = new Label("Account Status");
        description.setFont(font);

        buttons.getChildren().addAll(optionsMessage,info,goShopping,quit);
        quit2.setOnAction(e -> primaryStage.setScene(login));

        //buttons

        Button buyB1 = new Button("Add to cart");
        Button buyB2 = new Button("Add to cart");
        Button buyB3 = new Button("Add to cart");
        Button buyB4 = new Button("Add to cart");
        Button buyAcc1 = new Button("Add to cart");
        Button buyAcc2 = new Button("Add to cart");
        Button buyAcc3 = new Button("Add to cart");
        Button buyAcc4 = new Button("Add to cart");
        Button buyAcc5 = new Button("Add to cart");
        Button buyAcc6 = new Button("Add to cart");
        Button buyAcc7 = new Button("Add to cart");
        Button buyAcc8 = new Button("Add to cart");

        //Changed all entries to match Database
        Text mug = new Text(" Name: Mug \n Cost: 12 CAD \n Description: Mug with the main character on it");
        Text tshirt = new Text(" Name: T-shirt \n Cost: 17 CAD \n Description: T-shirt with the main character on it");
        Text hoodie = new Text(" Name: Hoodie \n Cost: 22 CAD \n Description: Hoodies with the emblem of the book");
        Text sticker = new Text(" Name: Sticker \n Cost: 7 CAD \n Description: Sticker with the main character on it");
        Text actionFigure = new Text(" Name: Action Figure \n Cost: 32 CAD \n Description: Figurine of the main character");
        Text pen = new Text(" Name: Action Figure \n Cost: 5 CAD \n Description: Pen with the emblem of the hero on it");
        Text bookLight = new Text(" Name: Book Light \n Cost: 35 CAD \n Description: A powerful book light to help you read in the dark");
        Text candle = new Text(" Name: Library Scented Candle \n Cost: 40 CAD \n Description: Remind yourself of the pleasant smell of books at home");
        Text book1 = new Text(" Name: Fitness and Health: Debunking Myths \n Cost: 70 \n Description: Did you know that a lot of \nthe things you hear about health are usually fake!! \n Type: Digital");
        Text book2 = new Text(" Name: A DeepDive into E-Sports\n Cost: 100 \n Description: While gamers and nerds alike were bullied back \n in the day they are viewed as celebritites today.\n Type: Physical");
        Text book3 = new Text(" Name: How Do You Talk With Humans? \n Cost: 140 \n Description: What are humans? Why do we want \nto talk to them? What is socializing? \n Digital");
        Text book4 = new Text(" Name: What IS Wrong with you? \n Cost: 20 \n Description: You have a problem... Let us fix that. Type: Digital");
         
        quit.setOnAction(e -> {
            primaryStage.setScene(preStore);
            isDatabase = false;
        });
        info.setOnAction(e -> {
            primaryStage.setScene(account);

            if(isDatabase){
                Connection conn = null;
                try {
                    conn = DriverManager.getConnection("jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca","A2043475","Benji2129*");
                } 
                catch (SQLException e1) {
                    e1.printStackTrace();
                }
                if(conn != null) {    
                    final Connection connFinal = conn;
                    String customerPrinted = "";
                    PreparedStatement retrievedData = null; 
                    String finCustomer = customerPrinted; 
                    customerIDDB = user.getText();
                    try{
                        retrievedData = connFinal.prepareStatement("SELECT * FROM ACCOUNT_DETAILS WHERE CUST_ID = " + customerIDDB);
                        ResultSet data = retrievedData.executeQuery();
                        while(data.next()){
                            customerPrinted = data.getString("cust_name") + "\nMembership type: " +
                            data.getString("medal") + "\nPhone number: " +
                            data.getString("cust_phone") + "\nEmail: " +
                            data.getString("cust_email") + "\nCard: " +
                            data.getString("card_num") + "\nBalance: " +
                            data.getString("balance") + "$\nPoints: " +
                            data.getString("points");
                        }
                        finCustomer = customerPrinted;
                        Text customerText = new Text(finCustomer);
                        customerText.setFont(itemsFont);
                        menuInfo.getChildren().addAll(description,customerText,quit2);
                        root3.getChildren().add(menuInfo);
                    }catch(SQLException e3){
                        e3.printStackTrace();
                    }
                

                PreparedStatement books = null;
                PreparedStatement accessories = null;
                try{
                    books = conn.prepareStatement("SELECT * FROM STORE_BOOKS");
                    accessories = conn.prepareStatement("SELECT * FROM STORE_ACCESSORIES");

                    ResultSet listAcc = accessories.executeQuery();
                    ResultSet listBooks = books.executeQuery();
                    List<Book> allBooks = new ArrayList<Book>();
                    List<Accessories> allAcc = new ArrayList<Accessories>();
                    Book book = null;
                    Accessories accessory = null;
                    while(listBooks.next()){
                        book = new Book(
                            listBooks.getString("book_ID"),
                            listBooks.getString("title"),
                            listBooks.getString("pub_ID"),
                            listBooks.getDouble("book_cost"),
                            listBooks.getDouble("book_retail"),
                            listBooks.getString("genre"),
                            listBooks.getString("description"),
                            listBooks.getInt("stock"),
                            listBooks.getString("type")
                        );
                        allBooks.add(book);
                    }
                    while(listAcc.next()) {
                        accessory = new Accessories(
                            listAcc.getString("acc_ID"),
                            listAcc.getString("acc_name"),
                            listAcc.getDouble("acc_cost"),
                            listAcc.getDouble("acc_retail"),
                            listAcc.getString("acc_description"),
                            listAcc.getInt("acc_stock")
                        );
                        allAcc.add(accessory);
                    }
                    List<Item> itemsAvailable = new ArrayList<Item>();
                    for(Book b: allBooks){
                        itemsAvailable.add(b);
                    }
                    for(Accessories a: allAcc){
                        itemsAvailable.add(a);
                    }
                    List<Book> booksGot = new ArrayList<Book>();
                    List<Accessories> accs = new ArrayList<Accessories>();
                    List<Book> cartBooks = new ArrayList<Book>();
                    List<Accessories> cartAccs = new ArrayList<Accessories>();

                    VBox itemsDisplay = new VBox(15);
                    VBox bookDisplay = new VBox(10);
                    VBox accDisplay = new VBox(10);
                    HBox combined = new HBox(30);

                    buyB1.setOnAction(e1 -> {           
                        cart.searchBook("1", itemsAvailable, booksGot, cartBooks);      
                    });
                    buyB2.setOnAction(e1 -> {           
                        cart.searchBook("2", itemsAvailable, booksGot, cartBooks);      
                    });
                    buyB3.setOnAction(e1 -> {           
                        cart.searchBook("3", itemsAvailable, booksGot, cartBooks);      
                    });
                    buyB4.setOnAction(e1 -> {           
                        cart.searchBook("4", itemsAvailable, booksGot, cartBooks);      
                    });
                    buyAcc1.setOnAction(e1 -> {           
                        cart.searchAccs("5", itemsAvailable, accs, cartAccs);      
                    });
                    buyAcc2.setOnAction(e1 -> {           
                        cart.searchAccs("6", itemsAvailable, accs, cartAccs);      
                    });
                    buyAcc3.setOnAction(e1 -> {           
                        cart.searchAccs("7", itemsAvailable, accs, cartAccs);      
                    });
                    buyAcc4.setOnAction(e1 -> {           
                        cart.searchAccs("8", itemsAvailable, accs, cartAccs);      
                    });
                    buyAcc5.setOnAction(e1 -> {           
                        cart.searchAccs("9", itemsAvailable, accs, cartAccs);      
                    });
                    buyAcc6.setOnAction(e1 -> {           
                        cart.searchAccs("10", itemsAvailable, accs, cartAccs);      
                    });
                    buyAcc7.setOnAction(e1 -> {           
                        cart.searchAccs("11", itemsAvailable, accs, cartAccs);      
                    });
                    buyAcc8.setOnAction(e1 -> {           
                        cart.searchAccs("12", itemsAvailable, accs, cartAccs);      
                    });

                    bookDisplay.getChildren().addAll(book1,book2,book3,book4);
                    accDisplay.getChildren().addAll(mug,tshirt,hoodie,sticker,actionFigure,pen,bookLight,candle);
                    combined.getChildren().addAll(bookDisplay,accDisplay);

                    Button quit3 = new Button("Go back to menu");
                    Button calculator = new Button("Calculate total price");
                    quit3.setOnAction(e1 -> {
                        primaryStage.setScene(options);
                        cartAccs.clear();
                        cartBooks.clear();
                    });
            
                    VBox basket = new VBox(15);
            
                    calculator.setOnAction(e7 -> {
                        
                        Text bookHeader = new Text("----------BOOKS IN CART----------");
                        for(Book bookInCart : cartBooks) {
                            booksInCart += bookInCart.toString();
                        } 
                        Text accHeader = new Text("----------ACCESSORIES IN CART----------");
                        for(Accessories accessoryInCart : cartAccs) {
                            accsInCart += accessoryInCart.toString();
                        }
                        Text bookCartText = new Text(booksInCart);
                        Text accCartText = new Text(accsInCart);
                        basket.getChildren().addAll(bookHeader,bookCartText,accHeader,accCartText);
                        primaryStage.setScene(payScreen);
                    });
            
                    itemsDisplay.getChildren().addAll(items,combined,quit3,calculator);
            
                    root4.getChildren().add(itemsDisplay);

                    VBox purchaseInfo = new VBox(15);

                    Text price = new Text("Here is your total before discounts:");
                    Text finalPrice = new Text("Here is your total after discounts:");
                    Label title = new Label("Checkout");
                    title.setFont(font);

                    //basket.setFont(itemsFont);
                    price.setFont(itemsFont);
                    finalPrice.setFont(itemsFont);

                    Button quitProgram = new Button("Quit Program");
                    purchaseInfo.getChildren().addAll(title, basket, price, finalPrice, quitProgram);
                    
                    quitProgram.setOnAction(e9 -> {
                        primaryStage.setScene(preStore);
                        isDatabase = false;
                    });

                    payment.getChildren().add(purchaseInfo);

                }catch(SQLException e5) {
                    e5.printStackTrace();
                }
            }
            }else{
            
            List<Path> paths = new ArrayList<Path>();
            Path p = Paths.get("src\\main\\java\\bookstore\\ExcelFiles\\customers.csv");
            paths.add(p);
            p = Paths.get("src\\main\\java\\bookstore\\ExcelFiles\\payment_info.csv");
            paths.add(p);
            p = Paths.get("src\\main\\java\\bookstore\\ExcelFiles\\memberships.csv");
            paths.add(p);

            List<String> lines = new ArrayList<String>();
            List<String> lines2 = new ArrayList<String>();
            List<String> lines3 = new ArrayList<String>();
            try {
                lines = Files.readAllLines(paths.get(0));
                lines2 = Files.readAllLines(paths.get(1));
                lines3 = Files.readAllLines(paths.get(2));
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            int payID = 0;
            String name = "";
            long phone = 0;
            String email = "";
            String card_num = "";
            double balance = 0;
            int points = 0;
            int memID = 0;
            String membership = "";

            for(String ln: lines){
                String[] pieces = ln.split(",");

                String custID = user.getText();

                if(Integer.parseInt(pieces[0]) == Integer.parseInt(custID)){
                    name = pieces[1];
                    phone = Long.parseLong(pieces[2]);
                    payID = Integer.parseInt(pieces[3]);
                    memID = Integer.parseInt(pieces[4]);
                    email = pieces[5];
                    points = Integer.parseInt(pieces[6]);
                }
            }
            for(String ln2: lines2){
                String[] pieces2 = ln2.split(",");

                if(Integer.parseInt(pieces2[0]) == payID){
                    card_num = pieces2[1];
                    balance = Double.parseDouble(pieces2[2]);
                }
            }
            for(String ln3: lines3){
                String[] pieces3 = ln3.split(",");

                if(Integer.parseInt(pieces3[0]) == memID){
                    membership = pieces3[1];
                }
            }

            String returnText = "Name: " + name + "\nPhone: " + phone + "\nEmail: " + email + "\nCard: " + card_num + "\nBalance: " + balance + "$\nMembership: " + membership + "\nPoints: " + points;
            Text accountInfo = new Text(returnText);

            accountInfo.setFont(itemsFont);

            menuInfo.getChildren().addAll(description, accountInfo, quit2);

            root3.getChildren().add(menuInfo);
            }});
            List<Item> itemsCart = new ArrayList<Item>();
            List<Item> itemsAvailable = new ArrayList<Item>();
            String pathToBooks = "src\\main\\java\\bookstore\\ExcelFiles\\books.csv";
            String pathToAccessories = "src\\main\\java\\bookstore\\ExcelFiles\\accessories.csv";
            cart = null;
            goShopping.setOnAction(e -> {
                try {
                    FileItemImporter bookImporter = new FileItemImporter();
                    List<Book> books = bookImporter.loadBooks(pathToBooks);
            
                    FileItemImporter accsImporter = new FileItemImporter();
                    List<Accessories> accs = accsImporter.loadAccessories(pathToAccessories);
            
                    for(Book b: books){
                        itemsAvailable.add(b);
                    }
                    for(Accessories a: accs){
                        itemsAvailable.add(a);
                    } 
                }
                catch(IOException e1) {
                    System.out.println(e1);
                }

                primaryStage.setScene(booksAvailable);
            });
                cart = new Cart(itemsCart, itemsAvailable);
                
                goShopping.setOnAction(e -> primaryStage.setScene(booksAvailable));

                root2.getChildren().addAll(buttons);
                //account page
                quit2.setOnAction(e -> {

                    menuInfo.getChildren().clear();

                    primaryStage.setScene(options);
                });
                //root3.getChildren().add(menuInfo);

                //items page and books
                VBox itemsDisplay = new VBox(15);
                VBox accDisplay = new VBox(15);
                VBox bookDisplay = new VBox(15);
                VBox buttonsAcc = new VBox(40);
                VBox buttonsBook = new VBox(60);
                HBox combined = new HBox(50);

                buttonsBook.getChildren().addAll(buyB1,buyB2,buyB3,buyB4);
                buttonsAcc.getChildren().addAll(buyAcc1,buyAcc2,buyAcc3,buyAcc4,buyAcc5,buyAcc6,buyAcc7,buyAcc8);

                mug.setFont(itemsFont);
                tshirt.setFont(itemsFont);
                hoodie.setFont(itemsFont);
                sticker.setFont(itemsFont);
                actionFigure.setFont(itemsFont);
                pen.setFont(itemsFont);
                bookLight.setFont(itemsFont);
                candle.setFont(itemsFont);
                book1.setFont(itemsFont);
                book2.setFont(itemsFont);
                book3.setFont(itemsFont);
                book4.setFont(itemsFont);
                List<Book> books = new ArrayList<Book>();
                List<Accessories> accs = new ArrayList<Accessories>();
                List<Book> cartBooks = new ArrayList<Book>();
                List<Accessories> cartAccs = new ArrayList<Accessories>();
                buyB1.setOnAction(e -> {           
                    cart.searchBook("1", itemsAvailable, books, cartBooks);      
                });
                buyB2.setOnAction(e -> {           
                    cart.searchBook("2", itemsAvailable, books, cartBooks);      
                });
                buyB3.setOnAction(e -> {           
                    cart.searchBook("3", itemsAvailable, books, cartBooks);      
                });
                buyB4.setOnAction(e -> {           
                    cart.searchBook("4", itemsAvailable, books, cartBooks);      
                });
                buyAcc1.setOnAction(e -> {           
                    cart.searchAccs("5", itemsAvailable, accs, cartAccs);      
                });
                buyAcc2.setOnAction(e -> {           
                    cart.searchAccs("6", itemsAvailable, accs, cartAccs);      
                });
                buyAcc3.setOnAction(e -> {           
                    cart.searchAccs("7", itemsAvailable, accs, cartAccs);      
                });
                buyAcc4.setOnAction(e -> {           
                    cart.searchAccs("8", itemsAvailable, accs, cartAccs);      
                });
                buyAcc5.setOnAction(e -> {           
                    cart.searchAccs("9", itemsAvailable, accs, cartAccs);      
                });
                buyAcc6.setOnAction(e -> {           
                    cart.searchAccs("10", itemsAvailable, accs, cartAccs);      
                });
                buyAcc7.setOnAction(e -> {           
                    cart.searchAccs("11", itemsAvailable, accs, cartAccs);      
                });
                buyAcc8.setOnAction(e -> {           
                    cart.searchAccs("12", itemsAvailable, accs, cartAccs);      
                });
                

                accDisplay.getChildren().addAll(book1,book2,book3,book4);
                bookDisplay.getChildren().addAll(mug,tshirt,hoodie,sticker,actionFigure,pen,bookLight,candle);

                combined.getChildren().addAll(bookDisplay,buttonsAcc,accDisplay,buttonsBook);

                Button quit3 = new Button("Go back to menu");
                Button calculator = new Button("Calculate total price");
                quit3.setOnAction(e -> {
                    primaryStage.setScene(options);
                    cartAccs.clear();
                    cartBooks.clear();
                });

                VBox basket = new VBox(15);

                calculator.setOnAction(e -> {
                    
                    Text bookHeader = new Text("----------BOOKS IN CART----------");
                    for(Book book : cartBooks) {
                        booksInCart += book.toString();
                    } 
                    Text accHeader = new Text("----------ACCESSORIES IN CART----------");
                    for(Accessories accessory : cartAccs) {
                        accsInCart += accessory.toString();
                    }
                    Text bookCartText = new Text(booksInCart);
                    Text accCartText = new Text(accsInCart);
                    basket.getChildren().addAll(bookHeader,bookCartText,accHeader,accCartText);
                    primaryStage.setScene(payScreen);
                });

                itemsDisplay.getChildren().addAll(items,combined,quit3,calculator);

                root4.getChildren().add(itemsDisplay);

                //PURCHASING

                VBox purchaseInfo = new VBox(15);

                
                Text price = new Text("Here is your total before discounts:");
                Text finalPrice = new Text("Here is your total after discounts:");
                Label title = new Label("Checkout");
                title.setFont(font);

                //basket.setFont(itemsFont);
                price.setFont(itemsFont);
                finalPrice.setFont(itemsFont);

                Button quitProgram = new Button("Quit Program");
                purchaseInfo.getChildren().addAll(title, basket, price, finalPrice, quitProgram);
                
                quitProgram.setOnAction(e -> {
                    primaryStage.setScene(preStore);
                    isDatabase = false;
                });

            payment.getChildren().add(purchaseInfo);
            
        stage.show();
    }
    /**
     * Main method to start application
     * @param args
     */
    public static void main(String[] args) {
        Application.launch(args);
    }
} 