package bookstore.Item;
//Book class holds all information about a book, is a child class of the Item abstract class

public class Book extends Item {
    
    private String bookID;
    private String bookTitle;
    private String pubID;
    private double bookCost;
    private double retail;
    private String genre;
    private String description;
    private int stock;
    private String type;

    /**
     * Constructor for the Book class to initialize all params to each field
     * @param bookID
     * @param bookTitle
     * @param pubID
     * @param bookCost
     * @param retail
     * @param genre
     * @param description
     * @param stock
     * @param type
     */
    public Book(String bookID, String bookTitle, String pubID, double bookCost, double retail, String genre, String description, int stock, String type) {
        this.bookID = bookID;
        this.bookTitle = bookTitle;
        this.pubID = pubID;
        this.bookCost = bookCost;
        this.retail = retail;
        this.genre = genre;
        this.description = description;
        this.stock = stock;
        this.type = type;
    }

    /**
     * @return bookTitle
     */
    public String getBookTitle() {
        return this.bookTitle;
    }

    /**
     * @return bookID
     */
    public String getBookID() {
        return this.bookID;
    }

    /**
     * @return pubID
     */
    public String getPubID() {
        return this.pubID;
    }

    /**
     * @return bookCost
     */
    public double getBookCost() {
        return this.bookCost;
    }

    /**
     * @return description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * @return genre
     */
    public String getGenre() {
        return this.genre;
    }
    public void setStock(int stock) {
        this.stock = stock;
    }

    /**
     * @return retail
     */
    public double getRetail() {
        return this.retail;
    }
    /**
     * @return stock
     */
    public int getStock() {
        return this.stock;
    }
    /**
     * @return type
     */
    public String getType() {
        return type;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        String printStatement = "";

        printStatement += ("\n Book Title: " + this.bookTitle + 
        "\n" + " Price: " + this.retail + "$\n" + " Genre: " + this.genre + "\n" + " Stock: " + this.stock
        + "\n" + " Type: " + this.type + "\n");

        return printStatement;
    }
}
