package bookstore.Item;
//Accessories class holds and handles all information about the accessories, child of the Item abstract class

public class Accessories extends Item {
    private String accesoryID;
    private String accessoryName;
    private double cost;
    private double retail;
    private String description;
    private int stock;

    /**
     * Constructor for the Accessory class to initialize all params to each field
     * @param accesoryID
     * @param accessoryName
     * @param cost
     * @param retail
     * @param description
     * @param stock
     */
    public Accessories(String accesoryID, String accessoryName, double cost, double retail, String description, int stock) {
        this.accesoryID = accesoryID;
        this.accessoryName = accessoryName;
        this.cost = cost;
        this.retail = retail;
        this.description = description;
        this.stock = stock;
    }

    /**
     * sets stock
     * @param stock
     */
    public void setStock(int stock) {
        this.stock = stock;
    }

    /**
     * @return accessoryID
     */
    public String getAccesoryID() {
        return accesoryID;
    }

    /**
     * @return cost
     */
    public double getCost() {
        return cost;
    }

    /**
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return retail
     */
    public double getRetail() {
        return retail;
    }
    /**
     * @return accessoryName
     */
    public String getAccesoryName() {
        return accessoryName;
    }
    /**
     * @return stock
     */
    public int getStock() {
        return stock;
    }

    /**
     * set accessoryID
     * @param accesoryID
     */
    public void setAccessoryId(String accesoryID) {
        this.accesoryID = accesoryID;
    }
    /**
     * set cost
     * @param cost
     */
    public void setCost(double cost) {
        this.cost = cost;
    }
    /**
     * set description
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * set retail
     * @param retail
     */
    public void setRetail(double retail) {
        this.retail = retail;
    }
    /**
     * set accessoryName
     * @param accesoryName
     */
    public void setAccesoryName(String accesoryName) {
        this.accessoryName = accesoryName;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        String accPrinted = "";

        accPrinted += "\nAccessory: " + this.accessoryName + "\n" + "Description: " + this.description + "\n" + "Price: "
        + this.retail + "\n" + "Stock: " + this.stock + "\n";
        return accPrinted;
    }
}
