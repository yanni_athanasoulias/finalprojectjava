package bookstore.Item;
//FileItemImporter is the class we use to create a list of Books or Items using a path.
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import bookstore.RewardsType.*;

public class FileItemImporter implements IItemImporter{
    /**
     * @param filePath - Provided path for a file containing books separated by /n (or just new line) and commas
     * Using the provided argument, we first read all lines of the path and then split each line, using a loop, into pieces. These pieces will form each Book inside of the List. 
     */
    @Override
    public List<Book> loadBooks(String filePath) throws IOException {
        Path p = Paths.get(filePath);
        List<String> lines = Files.readAllLines(p);
        ArrayList<Book> books = new ArrayList<Book>();

        for(String line: lines) {
            String[] pieces = line.split(",");

            books.add(new Book(pieces[0],pieces[1],pieces[2],Double.parseDouble(pieces[3]), Double.parseDouble(pieces[4]),pieces[5], pieces[6], Integer.parseInt(pieces[7]),pieces[8]));
        }
        return books;
    }
    /**
     * @param filePath - Provided path for a file containing accessories separated by /n (or just new line) and commas
     * Same functionality as above, just adjusted number and type of pieces for accessories.
     */
    @Override
    public List<Accessories> loadAccessories(String filePath) throws IOException {
        Path p = Paths.get(filePath);
        List<String> lines = Files.readAllLines(p);
        ArrayList<Accessories> accs = new ArrayList<Accessories>();

        for(String line: lines) {
            String [] pieces = line.split(",");

            accs.add(new Accessories(pieces[0], pieces[1],Double.parseDouble(pieces[2]), Double.parseDouble(pieces[3]),
            pieces[4], Integer.parseInt(pieces[5])));
        }
        return accs;
    }
    
}
