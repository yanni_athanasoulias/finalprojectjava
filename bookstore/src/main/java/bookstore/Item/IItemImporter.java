package bookstore.Item;

import java.io.IOException;
import java.util.List;
// import bookstore.RewardsType.*;

/**
 * This interface provides the two functions that will be used to import the books and the accessories 
 * in the fileItemImporter class by using the file paths.
 */
public interface IItemImporter {
    List<Book> loadBooks(String filePath) throws IOException;
    List<Accessories> loadAccessories(String filePath) throws IOException;
    // List<Coupons> loadCoupons(String filePath) throws IOException;
}
