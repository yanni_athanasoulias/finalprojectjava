package bookstore.Application;

import bookstore.ExternalClasses.Cart;
import bookstore.ExternalClasses.CustomerImporter;
import bookstore.ExternalClasses.Online_Orders;
import bookstore.ExternalClasses.Orders;
import bookstore.ExternalClasses.Payment;
import bookstore.ExternalClasses.Customer;
import bookstore.Item.Accessories;
import bookstore.Item.Book;
import bookstore.Item.FileItemImporter;
import bookstore.Item.Item;
import bookstore.MembershipType.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.sql.*;

/**
 * @author Karekin Kiyici, Yanni Athanasoulias and Hernan Mathias Farina Forster
 * The objective of this program is to store and read both from the database and the csv files to
 * process transactions while also applying coupons, discounts and points based on memberships.
 * @version 2022-11-21
 */

public class BookStore 
{
    static Scanner userInput = new Scanner(System.in);
    /**
     * The main method brings all the code together to create a working cli bookstore
     * 
     * @param args
     * @throws Exception
     */
    public static void main(String args[]) throws Exception {

        Cart cart = null;
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        System.out.println("\n       ---------------      MONTREAL BOOKSTORE      --------------- \n");
        System.out.println("\n                  -->   Enter your customer ID:   <--              \n");
        int customerID = userInput.nextInt();
        System.out.println("Press 1 to use database or press 2 to use the csv files");
        int databaseOrCsv = userInput.nextInt();

        if(databaseOrCsv == 1) {
            System.out.println( "\n   ------------------------- DATABASE -----------------------  \n");
            System.out.println("Enter your username:");
            String username = userInput.next();
            System.out.println("Enter your password:");
            String password = userInput.next();
            Connection conn = DriverManager.getConnection(url, username, password);
            System.out.println("Press 1 to add something to the cart or press 2 to check your account status");
            int accountOrCart = userInput.nextInt();
            if(accountOrCart == 1) {
                cart = addToCartDB(conn);
                Customer cust = getCustomerFromDB(conn, customerID);
                double price = userChoosePriceOption(cart, cust);
                Payment pay = new Payment();
                boolean hasFunds = pay.checkIfEnoughMoney(price);
                //Here we call a method that will do a few things: check if hasFunds is true, if not then call addFunds(). If true, will go to purchase. Once purchased,
                //we will have to update the csv/database accordingly and close the program? Anyway, if we have the time to include coupons, we also should then include coupons
                //as a bonus reward for purchasing (random chance for random coupon) and then ask the customer in the userChoosePriceOption() function if they want to use one.
                purchaseHandler(cart, cust, price, pay, hasFunds, conn);
            }else if(accountOrCart == 2) {
                showAccountStatus(conn, customerID);
                System.out.println("\n Type YES if you want to add funds to your account \n");
                updateBalanceDB(conn,customerID);
            }
        } 
        else if(databaseOrCsv == 2) {
            System.out.println( "\n       ------------------------- CSV -----------------------     \n");
            System.out.println("Press 1 to add something to the cart, press 2 to check your account status");
            int accountOrCart = userInput.nextInt();
            if(accountOrCart == 1) {
                cart = addtoCartCSV();
                Customer cust = new Customer();
                cust = cust.loadAccountDetails(customerID, false);
                addPointsForCustomer(cart, cust);
                double price = userChoosePriceOption(cart, cust);
                Payment pay = new Payment();
                boolean hasFunds = pay.checkIfEnoughMoney(price);
                //Here we call a method that will do a few things: check if hasFunds is true, if not then call addFunds(). If true, will go to purchase. Once purchased,
                //we will have to update the csv/database accordingly and close the program? Anyway, if we have the time to include coupons, we also should then include coupons
                //as a bonus reward for purchasing (random chance for random coupon) and then ask the customer in the userChoosePriceOption() function if they want to use one.
                purchaseHandler(cart, cust, price, pay, hasFunds, null);
                
            } 
            else if(accountOrCart == 2) {
                Customer imp = new Customer();
                System.out.println(imp.loadAccountDetails(customerID, true));
                System.out.println(" Do you now want to add something to the cart? Type YES if that is the case.");
                String response = userInput.next();
                if(response.equals("YES")) {
                    cart = addtoCartCSV();
                    Customer cust = new Customer();
                    cust = cust.loadAccountDetails(customerID, false);
                    double price = userChoosePriceOption(cart, cust);
                    Payment pay = new Payment();
                boolean hasFunds = pay.checkIfEnoughMoney(price);
                //Here we call a method that will do a few things: check if hasFunds is true, if not then call addFunds(). If true, will go to purchase. Once purchased,
                //we will have to update the csv/database accordingly and close the program? Anyway, if we have the time to include coupons, we also should then include coupons
                //as a bonus reward for purchasing (random chance for random coupon) and then ask the customer in the userChoosePriceOption() function if they want to use one.
                purchaseHandler(cart, cust, price, pay, hasFunds, null);
                }
            }
        }   
    }
    /**
     * This method takes in a connection and a customer ID to get all the info needed
     * from the database to create a Customer object.
     * @param conn
     * @param custID
     * @return
     * @throws SQLException
     */
    public static Customer getCustomerFromDB(Connection conn, int custID) throws SQLException{
        Customer cust= null;

        PreparedStatement custData = null;
        try{
            custData = conn.prepareStatement("SELECT C.CUST_NAME, C.CUST_PHONE, C.PAY_ID, C.MEM_TYPE,  C.CUST_EMAIL, P.POINTS FROM STORE_CUSTOMERS C INNER JOIN STORE_PAYMENT_INFO P ON P.PAY_ID = C.PAY_ID WHERE CUST_ID = " + custID);
            ResultSet data = custData.executeQuery();
            while(data.next()){
                cust = new Customer(custID, 
                    data.getString("cust_name"),
                    data.getLong("cust_phone"),
                    data.getInt("pay_id"),
                    data.getInt("mem_type"),
                    data.getString("cust_email"),
                    data.getInt("points"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            if(custData != null && !custData.isClosed()){
                custData.close();
            }
        }

        return cust;
    }
    /**
     * User chooses items printed using this method
     * @param cart
     * @return
     * @throws Exception
     */
    public static Cart chooseItems(Cart cart) throws Exception {
        boolean browsing = true;
        boolean decided = false;
        System.out.println("\n\n\n NOTE ---> Press -1 when stop adding products to your cart\n");
        while(browsing){
            while(!decided){
                int productSelected;
                try{ 
                    System.out.println("\n          -----------Which product would you like to add to your cart? Select it by entering the item id.----------        \n");
                    productSelected = Integer.parseInt(userInput.next());
                    String product = productSelected + "";
                    if(productSelected != -1 && productSelected < 0){
                        System.out.println("Please type a positive number");
                    }else{
                        if(productSelected == -1) {
                            decided = true;
                            browsing = false;
                        } else {
                            cart.addItemToCart(product);
                        }
                    }
                }catch(NumberFormatException e){
                    System.out.println("Please type in a number");
                }catch(IndexOutOfBoundsException f){
                    System.out.println("ID is not part of the list!");
                }
            }
        }
        return cart;
    }
    
    /**
     * @return Cart from the CSV files of items and books using the fileImporter classes
     * @throws Exception
     */
    public static Cart addtoCartCSV() throws Exception {

        String pathToBooks = "src\\main\\java\\bookstore\\ExcelFiles\\books.csv";
        String pathToAccessories = "src\\main\\java\\bookstore\\ExcelFiles\\accessories.csv";
        Cart itemsInCart = null;

        try {
            FileItemImporter bookImporter = new FileItemImporter();
            List<Book> books = bookImporter.loadBooks(pathToBooks);

            FileItemImporter accsImporter = new FileItemImporter();
            List<Accessories> accs = accsImporter.loadAccessories(pathToAccessories);

            List<Item> itemsAvailable = new ArrayList<Item>();
            List<Item> itemsCart = new ArrayList<Item>();
            for(Book b: books){
                itemsAvailable.add(b);
            }
            for(Accessories a: accs){
                itemsAvailable.add(a);
            }

            Cart cart = new Cart(itemsCart, itemsAvailable);

            cart.printItems();

            itemsInCart = chooseItems(cart);  
        }
        catch(IOException e) {
            System.out.println(e);
        }
        return itemsInCart;
    }
    /**
     * Prints account details according to the customer using a view made in sql.
     * @param conn
     * @param cust
     * @throws SQLException
     */
    public static void showAccountStatus(Connection conn, int custID) throws SQLException{

        PreparedStatement retrievedData = null;
        try{
            retrievedData = conn.prepareStatement("SELECT * FROM ACCOUNT_DETAILS WHERE CUST_ID = " + custID);
            ResultSet data = retrievedData.executeQuery();
            while(data.next()){
                System.out.println(
                    data.getString("cust_name") + "\nMembership type: " +
                    data.getString("medal") + "\nPhone number: " +
                    data.getString("cust_phone") + "\nEmail: " +
                    data.getString("cust_email") + "\nCard: " +
                    data.getString("card_num") + "\nBalance: " +
                    data.getString("balance") + "$\nPoints: " +
                    data.getString("points")
                );
            }

        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            if(retrievedData != null && !retrievedData.isClosed()){
                retrievedData.close();
            }
        }
    }

    /**
     * @param conn
     * @return Cart from the database
     * @throws Exception
     */
    public static Cart addToCartDB(Connection conn) throws Exception {
        
        Cart itemsAdded = null;

        PreparedStatement books = null;
        PreparedStatement accessories = null;
        try{
            books = conn.prepareStatement("SELECT * FROM STORE_BOOKS");
            accessories = conn.prepareStatement("SELECT * FROM STORE_ACCESSORIES");

            ResultSet listAcc = accessories.executeQuery();
            ResultSet listBooks = books.executeQuery();
            List<Book> allBooks = new ArrayList<Book>();
            List<Accessories> allAcc = new ArrayList<Accessories>();
            Book book = null;
            Accessories accessory = null;
            while(listBooks.next()){
                book = new Book(
                    listBooks.getString("book_ID"),
                    listBooks.getString("title"),
                    listBooks.getString("pub_ID"),
                    listBooks.getDouble("book_cost"),
                    listBooks.getDouble("book_retail"),
                    listBooks.getString("genre"),
                    listBooks.getString("description"),
                    listBooks.getInt("stock"),
                    listBooks.getString("type")
                );
                allBooks.add(book);
            }
            while(listAcc.next()) {
                accessory = new Accessories(
                    listAcc.getString("acc_ID"),
                    listAcc.getString("acc_name"),
                    listAcc.getDouble("acc_cost"),
                    listAcc.getDouble("acc_retail"),
                    listAcc.getString("acc_description"),
                    listAcc.getInt("acc_stock")
                );
                allAcc.add(accessory);
            }
            List<Item> itemsAvailable = new ArrayList<Item>();
            List<Item> itemsCart = new ArrayList<Item>();
            for(Book b: allBooks){
                itemsAvailable.add(b);
            }
            for(Accessories a: allAcc){
                itemsAvailable.add(a);
            }

            Cart cart = new Cart(itemsCart, itemsAvailable);
            cart.printItems();
            itemsAdded = chooseItems(cart);

            return itemsAdded;
            
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            if(books != null && !books.isClosed()){
                books.close();
            }
            if(accessories != null && !accessories.isClosed()){
                accessories.close();
            }
        }
        return itemsAdded;
    }

    /**
     * @param cart
     * @param cust
     * @return The final price (double) of everything put together: Cost of items in cart, discounts,
     * point calculations, point discounts, seperate membership options, as well shipping costs.
     * Shipping info also comes with diserning whether the books are ebook or physical
     */
    public static double userChoosePriceOption(Cart cart,Customer cust) {
        double cartOnlyPrice = priceOfCartOnlyItems(cart);

        cart.printsCart();

        boolean addShipCost = false;

        System.out.println("\n----------          Here is the total price of your cart before membership discounts            ------------\n");
        
        System.out.println("Current price: " + cartOnlyPrice);

        System.out.println("\n----------                  Now it is time to add the membership discounts!                     ------------\n");

        if (cust.getMembershipID() == 1) {

            System.out.println("\nOof, turns out that you don't get any discounts...\n");

        } else if (cust.getMembershipID() == 2) {

            Membership bronze = new Bronze();

            cartOnlyPrice = bronze.calculateDiscount(cartOnlyPrice, 0.05);
            for(Item i: cart.getCart()){
                if(i instanceof Book){
                    Book bi = (Book) i;
                    if(bi.getType().equals("Physical")){
                        addShipCost = true;
                        break;
                    }
                }else if(i instanceof Accessories){
                    addShipCost = true;
                    break;
                }
            }

        } else if (cust.getMembershipID() == 3) {

            Membership silver = new Silver();

            cartOnlyPrice = silver.calculateDiscount(cartOnlyPrice, 0.10);
            for(Item i: cart.getCart()){
                if(i instanceof Book){
                    Book bi = (Book) i;
                    if(bi.getType().equals("Physical")){
                        addShipCost = true;
                        break;
                    }
                }else if(i instanceof Accessories){
                    addShipCost = true;
                    break;
                }
            }

        } else if (cust.getMembershipID() == 4) {

            Membership gold = new Gold();

            cartOnlyPrice = gold.calculateDiscount(cartOnlyPrice, 0.15);
            for(Item i: cart.getCart()){
                if(i instanceof Book){
                    Book bi = (Book) i;
                    if(bi.getType().equals("Physical")){
                        addShipCost = true;
                        break;
                    }
                }else if(i instanceof Accessories){
                    addShipCost = true;
                    break;
                }
            }

        }

        int amountOfPoints = cust.getPoints();

        if (amountOfPoints >= 20) {
            System.out.println("Would you like to use 20 points to get 10$ off of your total?");
            System.out.println("Input Yes if you would like to use 20 points, No if not");
            Scanner reader = new Scanner(System.in);
            String answer = reader.nextLine();
            if (answer.equals("Yes")) {
                cartOnlyPrice -= 10;
            }
            reader.close();
        }

        double finalPrice = cartOnlyPrice + 2;
        if(addShipCost)finalPrice += 3;
        System.out.println(" This is the total price of your cart including taxes and shipping: " + finalPrice + "\n");
        return finalPrice;
    }

    /**
     * @param cart
     * @return The total price of only the items in the cart, nothing else, no discounts nothing,
     * just the cart contents
     */
    public static double priceOfCartOnlyItems(Cart cart) {
        List<Item> customerCart = cart.getCart();

        System.out.println("Size of cart: " + customerCart.size());

        double onlyItemsPrice = 0;

            for (Item bc: customerCart) {
                if(bc instanceof Accessories) {
                    Accessories accs = (Accessories) bc;
                    onlyItemsPrice += accs.getRetail();
                }
                if(bc instanceof Book) {
                    Book books = (Book) bc;
                    onlyItemsPrice += books.getRetail();
                }
            }
        return onlyItemsPrice;
    }

    /**
     * This method takes the cart and a customer and it sets that customer's points total using the
     * addToPointsBasedOnMembership method from the membership subtypes
     * @param cart
     * @param cust
     */
    public static void addPointsForCustomer(Cart cart, Customer cust) {
        int pointsAmount = cust.getPoints();

        System.out.println("\n----------                Here is the total amount of points you currently hold                 ------------\n");
        
        System.out.println("Current point count: " + pointsAmount);

        System.out.println("\n----------                  Now it is time to add the points based on you membership!           ------------\n");

            if (cust.getMembershipID() == 1) {

                Membership base = new Base();

                pointsAmount = base.addToPointsBasedOnMembership(cart.printSize());

                cust.setPoints(cust.getPoints() + pointsAmount);

            } else if (cust.getMembershipID() == 2) {

                Membership bronze = new Bronze();

                pointsAmount = bronze.addToPointsBasedOnMembership(cart.printSize());

                cust.setPoints(cust.getPoints() + pointsAmount);

            } else if (cust.getMembershipID() == 3) {

                Membership silver = new Silver();

                pointsAmount = silver.addToPointsBasedOnMembership(cart.printSize());

                cust.setPoints(cust.getPoints() + pointsAmount);

            } else if (cust.getMembershipID() == 4) {

                Membership gold = new Gold();

                pointsAmount = gold.addToPointsBasedOnMembership(cart.printSize());

                cust.setPoints(cust.getPoints() + pointsAmount);

            }

        System.out.println("After checking the size of your cart and what membership you have, this is your new point total: " + pointsAmount);

        cust.setPoints(pointsAmount);
    }

    /**
     * Handles all purchase info that come from a transaction
     * @param cart
     * @param cust
     * @param price
     * @param pay
     * @param hasFunds
     * @param conn
     * @throws IOException
     * @throws SQLException
     */
    public static void purchaseHandler(Cart cart, Customer cust, double price, Payment pay, boolean hasFunds, Connection conn) throws IOException, SQLException{
        if(!hasFunds){
            if(conn == null){
                addFunds(cust, price, "bookstore\\src\\main\\java\\bookstore\\ExcelFiles\\payment_info.csv");
            }else{
                updateBalanceDB(conn, cust.getCustomerID());
            }
        }

        System.out.println("Thank you for purchasing from us!");

        Cart shipCart;

        List<Item> cartItems = new ArrayList<Item>();

        for(Item i: cart.getCart()){
            if(i instanceof Book){
                Book bi = (Book) i;
                if(bi.getType().equals("Physical")){
                    cartItems.add(bi);
                }
            }else if(i instanceof Accessories){
                cartItems.add(i);
            }
        }

        List<Item> inventoryItems = cart.getInventory();
        shipCart = new Cart(cartItems, inventoryItems);

        Orders order = new Orders("1", "19-SEP-2022", "22-SEP-2022", "600 avenue crest Jermantown Quebec Canada");

        Online_Orders o_orders = new Online_Orders("1", "22-NOV-2022");

        String[] paths = {"bookstore\\src\\main\\java\\bookstore\\ExcelFiles\\payment_info.csv", "bookstore\\src\\main\\java\\bookstore\\ExcelFiles\\customers.csv", "bookstore\\src\\main\\java\\bookstore\\ExcelFiles\\orders.csv", "bookstore\\src\\main\\java\\bookstore\\ExcelFiles\\online_orders.csv"};

        completePurchase(cust, order, o_orders, paths, price);


    }

    /**
     * Adds funds to the customer's total balance so they can purchase more items
     * @param cust
     * @param price
     * @param filepath
     * @throws IOException
     */
    public static void addFunds(Customer cust, double price, String filepath) throws IOException{
        int custID = cust.getCustomerID();
        cust = cust.loadAccountDetails(custID, true);
        int oldPayID = cust.getPaymentID();

        System.out.println("Would you like to use your card to add funds? Type YES if you do.");
        String answer = userInput.next();
        if(answer.equalsIgnoreCase("Yes")){
                Path p = Paths.get(filepath);
                List<String> lines = Files.readAllLines(p);

                String card = "";

                for(String ln: lines){
                    String[] pieces = ln.split(",");
                    if(Integer.parseInt(pieces[0]) == oldPayID) card = pieces[1];
                }

                String tempFile = "payment_info.csv";
                File oldFile = new File(filepath);
                File newFile = new File(tempFile);
                int payIDs = 0;
                String cards = "";
                double balances = 0.0;

                try{
                    FileWriter fw = new FileWriter(tempFile, true);
                    BufferedWriter bw = new BufferedWriter(fw);
                    PrintWriter pw = new PrintWriter(bw);
                    Scanner fileScan = new Scanner(new File(filepath));
                    fileScan.useDelimiter("[,\n]");

                    while(fileScan.hasNext()){
                        payIDs = Integer.parseInt(fileScan.next());
                        cards = fileScan.next();
                        balances = Double.parseDouble(fileScan.next());
                        if(payIDs == oldPayID){
                            pw.println(oldPayID + "," + card + "," + price);
                        }else{
                            pw.println(payIDs + "," + cards + "," + balances);
                        }
                    }
                    fileScan.close();
                    pw.flush();
                    pw.close();
                    oldFile.delete();
                    File dump = new File(filepath);
                    newFile.renameTo(dump);

                }finally{

                }
        }
    }

    public static void completePurchase(Customer cust, Orders order, Online_Orders o_orders, String[] paths, double price) throws IOException{
        int custID = cust.getCustomerID();
        cust = cust.loadAccountDetails(custID, true);
        int oldPayID = cust.getPaymentID();

        Path p = Paths.get(paths[0]);
        List<String> lines = Files.readAllLines(p);

        String card = "";
        double newBalance = 0;

        String old_name = cust.getName();
        long old_phone = cust.getPhoneNumber();
        int old_pay_ID = cust.getPaymentID();
        int old_mem_ID = cust.getMembershipID();
        String old_email = cust.getEmailAddress();
        int old_points = cust.getPoints();
        
        String old_orderNum = order.getOrder_num();
        String old_order_date = order.getOrder_date();
        String old_ship_date = order.getShip_date();
        String old_address = order.getAddress();

        String old_o_orderNum = o_orders.getOrder_num();
        String old_o_order_date = o_orders.getOrder_date();

        for(String ln: lines){
            String[] pieces = ln.split(",");
            if(Integer.parseInt(pieces[0]) == oldPayID){
                card = pieces[1];
                newBalance = Double.parseDouble(pieces[2]) - price;
            }
        }

        String tempFile = "payment_info.csv";
        File oldFile = new File(paths[0]);
        File newFile = new File(tempFile);
        int payIDs = 0;
        String cards = "";
        double balances = 0.0;

        String tempFileC = "customers.csv";
        File oldFileC = new File(paths[1]);
        File newFileC = new File(tempFileC);
        int custIDs = 0;
        String names = "";
        long phones = 0;
        int pay_IDs = 0;
        int mem_IDs = 0;
        String emails = "";
        int pointss = 0;
        
        String tempFileO = "orders.csv";
        File oldFileO = new File(paths[2]);
        File newFileO = new File(tempFileO);
        String orderNums = "";
        String order_dates = "";
        String ship_dates = "";
        String addresses = "";

        String tempFileOO = "online_orders.csv";
        File oldFileOO = new File(paths[3]);
        File newFileOO = new File(tempFileC);
        String o_orderNums = "";
        String o_order_dates = "";


        try{
            
            FileWriter fw = new FileWriter(tempFile, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);

            //UPDATING PAYMENT_INFO
            Scanner fileScan = new Scanner(new File(paths[0]));
            fileScan.useDelimiter("[,\n]");

            while(fileScan.hasNext()){
                payIDs = Integer.parseInt(fileScan.next());
                cards = fileScan.next();
                balances = Double.parseDouble(fileScan.next());
                if(payIDs == oldPayID){
                    pw.println(oldPayID + "," + card + "," + newBalance);
                }else{
                    pw.println(payIDs + "," + cards + "," + balances);
                }
            }

            //UPDATING CUSTOMERS
            fileScan = new Scanner(new File(paths[1]));
            fileScan.useDelimiter("[,\n]");

            while(fileScan.hasNext()){
                custIDs = Integer.parseInt(fileScan.next());
                names = fileScan.next();
                phones = Long.parseLong(fileScan.next());
                pay_IDs = Integer.parseInt(fileScan.next());
                mem_IDs = Integer.parseInt(fileScan.next());
                emails = fileScan.next();
                pointss = Integer.parseInt(fileScan.next());
                if(custIDs == custID){
                    pw.println(custID + "," + old_name + "," + old_phone + "," + old_pay_ID + "," + old_mem_ID + "," + old_email + "," + old_points);
                }else{
                    pw.println(custIDs + "," + names + "," + phones + "," + pay_IDs + "," + mem_IDs + "," + emails + "," + pointss);
                }
            }

            //UPDATING ORDERS
            fileScan = new Scanner(new File(paths[2]));
            fileScan.useDelimiter("[,\n]");

            while(fileScan.hasNext()){
                orderNums = fileScan.next();
                order_dates = fileScan.next();
                ship_dates = fileScan.next();
                addresses = fileScan.next();
                if(custIDs == custID){
                    pw.println(old_orderNum + "," + old_order_date + "," + old_ship_date + "," + old_address);
                }else{
                    pw.println(orderNums + "," + order_dates + "," + ship_dates + "," + addresses);
                }
            }

            //UPDATING ONLINE_ORDERS
            fileScan = new Scanner(new File(paths[3]));
            fileScan.useDelimiter("[,\n]");

            while(fileScan.hasNext()){
                o_orderNums = fileScan.next();
                o_order_dates = fileScan.next();
                if(custIDs == custID){
                    pw.println(old_o_orderNum + "," + old_o_order_date);
                }else{
                    pw.println(o_orderNums + "," + o_order_dates);
                }
            }

            fileScan.close();
            pw.flush();
            pw.close();
            oldFile.delete();
            File dump = new File(paths[0]);
            newFile.renameTo(dump);


        }finally{

        }
    }
    
    /**
     * Updates balance in the database
     * @param conn
     * @param customerID
     * @throws SQLException
     */
    public static void updateBalanceDB(Connection conn, int customerID) throws SQLException {

        CallableStatement stmt = null;

        try {
            System.out.println("Enter the new balance:");
            int newBalance = userInput.nextInt();

            stmt = conn.prepareCall("{call update_balance(?,?)}");
            stmt.setInt(1, customerID);
            stmt.setInt(2, newBalance);
            stmt.execute();

            System.out.println("\n NOTE: Funds have been updated successfully\n");
        } 
        catch(SQLException e) {
            e.printStackTrace();
        } finally {
            stmt.close();
        }
    }

    /**
     * Updates the new Points in the Database
     * @param conn
     * @param customerID
     * @param points
     * @throws SQLException
     */
    public static void updatePointsDB(Connection conn, int customerID, int points) throws SQLException {

        CallableStatement stmt = null;

        try {
            stmt = conn.prepareCall("{call update_points(?,?)}");
            stmt.setInt(1, customerID);
            stmt.setInt(2, points);
            stmt.execute();

            System.out.println("\n ->>>>> You have won points!!! <<<<<- \n");
        } 
        catch(SQLException e) {
            e.printStackTrace();
        } finally {
            stmt.close();
        }
    }
}