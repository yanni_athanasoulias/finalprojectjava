package bookstore.MembershipType;
//Base class is the free membership that all users have access to, or in other words the default.
//For example, if the user chooses not to be a bronze, silver, or gold member then they are automatically set as a base member

import bookstore.ExternalClasses.*;
import bookstore.RewardsType.*;

import java.io.IOException;
import java.util.List;

public class Base extends Membership {

    /**
     * This will be the constructor that will be needed for the sql code to work
     */
    public Base() {

    }   
    
    /* (non-Javadoc)
     *toString method which prints a base membership message
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return "This is the Base Memebership, if you want you can purchase the Bronze for, Silver for, Gold for";
    }

    /* (non-Javadoc)
     *returns int of the Points a customer will receive based on their cartSizes
     * @see bookstore.MembershipType.Membership#addToPointsBasedOnMembership(int)
     */
    @Override
    public int addToPointsBasedOnMembership(int cartSize) {
        System.out.println("Welcome to our Book Store! As a base member you will be able to get 2 points for every 5 items in your cart");
        int addtoPoints = 0;
            for (int i = 0; i <= cartSize;i++) {
                if (i%5 == 0 && i > 4) {
                    addtoPoints+=2;
                }
            }
        return addtoPoints;
    }
}
