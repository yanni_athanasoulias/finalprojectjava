package bookstore.MembershipType;
//Gold class is the most expensive membership option. Users have access to everything the others have, as well as exclusive rewards

import bookstore.RewardsType.*;
import bookstore.ExternalClasses.Cart;
import bookstore.Item.*;

import java.io.IOException;
import java.util.List;

public class Gold extends Membership{
    private Coupons coupons;

    /**
     * Constructor so the sql can work
     */
    public Gold() {

    }

     /* (non-Javadoc)
     *returns int of the Points a customer will receive based on their cartSizes
     * @see bookstore.MembershipType.Membership#addToPointsBasedOnMembership(int)
     */
    @Override
    public int addToPointsBasedOnMembership(int cartSize) {
        int addtoPoints = 0;
        System.out.println("A GOLD MEMBER!!!! WOW!!! I didn't expect to see one of you guys here... well since you're a gold member you get the best rewards!!!");
        System.out.println("For EVERY ITEM in your cart you will get 2 points, now that's a great reward if you ask me");
        System.out.println("Not only that, but if your cart holds at least 5 items, you will get an extra 10 points!!!");
        System.out.println("And finally, as a gold member you get an exclusive reward, if your cart has at least 10 items then you will get... 20 points!!!");
        for (int i = 0;i <= cartSize; i++) {
                addtoPoints+=3;
        }
        if (cartSize > 4) {
            addtoPoints+=10;
        }
        if (cartSize >= 10) {
            addtoPoints+=20;
        }
        return addtoPoints;
    }
}
