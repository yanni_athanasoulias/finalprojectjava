package bookstore.MembershipType;
//Membership abstract class/base type holds all the membership info options and their info
//sub-classes are: Base, Bronze, Silver, Gold

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import bookstore.ExternalClasses.*;

import bookstore.RewardsType.*;

public abstract class Membership {
    /**
     * @return finalprice, which is the price the customer pays for the items after the discounts and taxes have been applied
     */
    public double calculateDiscount(double currentCartPrice,double discountPercentage) {
        currentCartPrice -= (currentCartPrice * discountPercentage);
        return currentCartPrice;
    }

     /* (non-Javadoc)
     *returns int of the Points a customer will receive based on their cartSizes
     * @see bookstore.MembershipType.Membership#addToPointsBasedOnMembership(int)
     */
    public int addToPointsBasedOnMembership(int cartSize) {
        return 0;
    }
}
