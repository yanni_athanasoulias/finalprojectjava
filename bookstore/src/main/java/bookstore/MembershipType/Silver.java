package bookstore.MembershipType;
//Silver class is the middle between gold and bronze. Has all of bronze, just better and more

import bookstore.RewardsType.*;

import java.io.IOException;
import java.util.List;

import bookstore.Item.*;
import bookstore.ExternalClasses.*;

public class Silver extends Membership{
    /**
     * Constructor for sql to work
     */
    public Silver() {

    }

     /* (non-Javadoc)
     *returns int of the Points a customer will receive based on their cartSizes
     * @see bookstore.MembershipType.Membership#addToPointsBasedOnMembership(int)
     */
    @Override
    public int addToPointsBasedOnMembership(int cartSize) {
        int addtoPoints = 0;
        System.out.println("A Silver member I see, impressive!!! For every 2 books you have in your cart, you will gain 3 points!");
        System.out.println("Also... If your cart holds more than 6 items, then you will gain an additional 10 points!!!");
        for (int i = 0;i <= cartSize; i++) {
            if (i%2 == 0 && i > 1) {
                addtoPoints+=3;
            }
        }
        if (cartSize > 6) {
            addtoPoints+=10;
        }
        return addtoPoints;
    }
}
