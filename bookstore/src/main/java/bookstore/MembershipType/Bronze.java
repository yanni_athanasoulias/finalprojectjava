package bookstore.MembershipType;
//Bronze class will be the cheapest memberhship option available. Bronze users have access to discounts, coupons, and points

import java.io.IOException;
import java.util.List;

import bookstore.ExternalClasses.Cart;
import bookstore.Item.*;
import bookstore.RewardsType.*;

public class Bronze extends Membership{

    /**
     * This constructor is for the sql to work
     */
    public Bronze() {

    }

     /* (non-Javadoc)
     *returns int of the Points a customer will receive based on their cartSizes
     * @see bookstore.MembershipType.Membership#addToPointsBasedOnMembership(int)
     */
    @Override
    public int addToPointsBasedOnMembership(int cartSize) {
        int addtoPoints = 0;
        System.out.println("Welcome Bronze Member!!!! For every 5 books that you have in your cart, you will gain 5 points:)");
        for (int i = 0; i <= cartSize; i++) {
             if (i%5 == 0 && i > 4) {
                addtoPoints+=5;
            }
        }
        return addtoPoints;
    }
}
