package bookstore.ExternalClasses;

public class Online_Orders {

    private String order_num;
    private String order_date;

    /**
     * Constructor which sets these params to the fields
     * @param num
     * @param date
     */
    public Online_Orders(String num, String date){
        this.order_num = num;
        this.order_date = num;
    }

    /**
     * @return order_num
     */
    public String getOrder_num() {
        return order_num;
    }

    /**
     * @return order_date
     */
    public String getOrder_date() {
        return order_date;
    }

    /**
     * set order_num
     * @param order_num
     */
    public void setOrder_num(String order_num) {
        this.order_num = order_num;
    }

    /**
     * set order_date
     * @param order_date
     */
    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }
}