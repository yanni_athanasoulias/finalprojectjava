package bookstore.ExternalClasses;
//Payment class will directly relate to the customers class as it holds and handles all the info for the payment methods

import bookstore.MembershipType.*;
import bookstore.RewardsType.*;

public class Payment {
    private double balance;
    private int payId;
    private String cardNum;

    /**
     * Empty Constructor
     */
    public Payment(){
        
    }

    /**
     * Constructor for the Payment class to initialize all params to each field
     * @param balance
     * @param payId
     * @param cardNum
     */
    public Payment(double balance, int payId, String cardNum) {
        this.balance = balance;
        this.payId = payId;
        this.cardNum = cardNum;
    }

    /**
     * @return balance
     */
    public double getBalance() {
        return balance;
    }

    /**
     * @return cardNum
     */
    public String getCardNum() {
        return cardNum;
    }
    
    /**
     * @return payID
     */
    public int getPayId() {
        return payId;
    }

    /**
     * set balance
     * @param balance
     */
    public void setBalance(double balance) {
        this.balance = balance;
    }

    /**
     * set cardNum
     * @param cardNum
     */
    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    /**
     * set payId
     * @param payId
     */
    public void setPayId(int payId) {
        this.payId = payId;
    }

    /**
     * @param cartTotal
     * @return boolean depending on whether the cartTotal is greater than that customer's balance
     */
    public boolean checkIfEnoughMoney(double cartTotal) {
        if(this.balance < cartTotal) {
            System.out.println("\n ---------------> Attention!!! You dont have enough funds to buy the item/s \n");
            return false;
        }

        return true;
    }
}
