package bookstore.ExternalClasses;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public interface CustomerImporter {
    //public List<Customer> loadCustomers(String filePath) throws IOException;
    public Customer loadAccountDetails(int custID, boolean PrintOrNot) throws IOException;
}
