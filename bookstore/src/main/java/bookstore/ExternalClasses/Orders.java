package bookstore.ExternalClasses;

public class Orders {
    
    private String order_num;
    private String order_date;
    private String ship_date;
    private String address;

    /**
     * Constructor which sets these params to the fields
     * @param num
     * @param date
     * @param s_date
     * @param address
     */
    public Orders(String num, String date, String s_date, String address){
        this.order_num = num;
        this.order_date = date;
        this.ship_date = s_date;
        this.address = address;
    }

    /**
     * @return order_num
     */
    public String getOrder_num() {
        return order_num;
    }

    /**
     * @return order_date
     */
    public String getOrder_date() {
        return order_date;
    }

    /**
     * @return ship_date
     */
    public String getShip_date() {
        return ship_date;
    }

    /**
     * @return address
     */
    public String getAddress() {
        return address;
    }

    /**
     * set order_num
     * @param order_num
     */
    public void setOrder_num(String order_num) {
        this.order_num = order_num;
    }

    /**
     * set order_date
     * @param order_date
     */
    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    /**
     * set ship_date
     * @param ship_date
     */
    public void setShip_date(String ship_date) {
        this.ship_date = ship_date;
    }

    /**
     * set address
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }
}
