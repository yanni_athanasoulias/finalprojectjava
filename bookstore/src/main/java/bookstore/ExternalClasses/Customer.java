package bookstore.ExternalClasses;

//Customer class holds and handles (i.e. through methods) all necessary information about each user/customer
//Uses Membership type
import bookstore.MembershipType.*;
import bookstore.Item.*;
import bookstore.RewardsType.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Customer implements CustomerImporter {
    
    private int customerID;
    private String name;
    private long phoneNumber;
    private int paymentID;
    private String emailAddress;
    private Cart customerCart;
    private int membershipID;
    private int points;
    private int coupID;
    private double balance;
    private String membership;

    /**
     * Constructor for the customer class to initialize all params to each field
     * @param customerID
     * @param name
     * @param membershipType
     * @param phoneNumber
     * @param paymentID
     * @param emailAddress
     * @param membership
     */
    public Customer(int customerID, String name, long phoneNumber, int paymentID,int membershipID, String emailAddress, int points) {
        this.customerID = customerID;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.paymentID = paymentID;
        this.emailAddress = emailAddress;
        this.points = points;
        this.membershipID = membershipID;
    }

    /**
     * @return membershipID
     */
    public int getMembershipID() {
        return membershipID;
    }

    /**
     * Empty Contructor
     */
    public Customer() {

    }

    /**
     * Constructor that sets all these params to the fields of the customer
     * @param customerID
     * @param name
     * @param phoneNumber
     * @param paymentID
     * @param membershipID
     * @param emailAddress
     * @param points
     * @param balance
     * @param membership
     */
    public Customer(int customerID, String name, long phoneNumber, int paymentID,int membershipID, String emailAddress, int points, double balance, String membership) {
        this.customerID = customerID;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.paymentID = paymentID;
        this.emailAddress = emailAddress;
        this.points = points;
        this.membershipID = membershipID;
        this.balance = balance;
        this.membership = membership;
    }

    /**
     * @return customerID
     */
    public int getCustomerID() {
        return customerID;
    }

    /**
     * @return emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * @return membership
     */

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @return paymentID
     */
    public int getPaymentID() {
        return paymentID;
    }

    /**
     * @return coupID
     */
    public int getCoupID() {
        return coupID;
    }

    /**
     * @return customerCart
     */
    public Cart getCustomerCart() {
        return this.customerCart;
    }

    /**
     * @return points
     */
    public int getPoints() {
        return this.points;
    }

    /**
     * @return phoneNumber
     */
    public long getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets customer phoneNumber
     * @param phone
     */
    public void setPhoneNumber(long phone){
        this.phoneNumber = phone;
    }

    /**
     * set customerID
     * @param customerID
     */
    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    /**
     * set emailAddress
     * @param emailAddress
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * Sets coupID
     * @param coupID
     */
    public void setCoupID(int coupID) {
        this.coupID = coupID;
    }

    /**
     * set name
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets MembershipID
     * @param membershipID
     */
    public void setMembershipID(int membershipID) {
        this.membershipID = membershipID;
    }

    /**
     * set paymentID
     * @param paymentID
     */
    public void setPaymentID(int paymentID) {
        this.paymentID = paymentID;
    }

    /**
     * set phoneNumber
     * @param phoneNumber
     */
    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * sets points
     * @param points
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * Prints account details according to the csv files instead of the database.
     * @param custID
     * @throws IOException
     */
    @Override
    public Customer loadAccountDetails(int custID,boolean printOrNot) throws IOException {

        Customer cust;

        List<Path> paths = new ArrayList<Path>();
        Path p = Paths.get("src\\main\\java\\bookstore\\ExcelFiles\\customers.csv");
        paths.add(p);
        p = Paths.get("src\\main\\java\\bookstore\\ExcelFiles\\payment_info.csv");
        paths.add(p);
        p = Paths.get("src\\main\\java\\bookstore\\ExcelFiles\\memberships.csv");
        paths.add(p);

        List<String> lines = Files.readAllLines(paths.get(0));
        List<String> lines2 = Files.readAllLines(paths.get(1));
        List<String> lines3 = Files.readAllLines(paths.get(2));

        int payID = 0;
        String name = "";
        long phone = 0;
        String email = "";
        String card_num = "";
        double balance = 0;
        int points = 0;
        int memID = 0;
        String membership = "";

        for(String ln: lines){
            String[] pieces = ln.split(",");

            if(Integer.parseInt(pieces[0]) == custID){
                name = pieces[1];
                phone = Long.parseLong(pieces[2]);
                payID = Integer.parseInt(pieces[3]);
                memID = Integer.parseInt(pieces[4]);
                email = pieces[5];
                points = Integer.parseInt(pieces[6]);
            }
        }
        for(String ln2: lines2){
            String[] pieces2 = ln2.split(",");

            if(Integer.parseInt(pieces2[0]) == payID){
                card_num = pieces2[1];
                balance = Double.parseDouble(pieces2[2]);
            }
        }
        for(String ln3: lines3){
            String[] pieces3 = ln3.split(",");

            if(Integer.parseInt(pieces3[0]) == memID){
                membership = pieces3[1];
            }
        }

        cust = new Customer(custID, name, phone, payID, memID, email, points, balance, membership);

        if(printOrNot) {
            System.out.println(" Name: " + name + "\n Membership Type: " + membership + "\n Phone number: " + phone + "\n Email: " + email + "\n Card: " + card_num + "\n Balance: " + balance + "$\n Points: " + points + "\n");
        }
        return cust;
    }
}
