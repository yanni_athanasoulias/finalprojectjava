package bookstore.ExternalClasses;

//Cart class will display to the user their items in the cart as well as displays the stock in the bookstore
//Uses Item base type
import bookstore.Item.*;
import bookstore.MembershipType.*;

import java.util.ArrayList;
import java.util.List;

public class Cart {
    
    //List<int, String> --- try later

    private List<Item> itemsAvailable;
    private List<Item> itemsCart;

    /**
     * Constructor for the cart class to initialize all params to each field
     * @param itemCart
     * @param itemsAvailable
     */
    public Cart(List<Item> itemsCart, List<Item> itemsAvailable) {
        this.itemsAvailable = itemsAvailable;
        this.itemsCart = itemsCart;
    }

    /**
     * Second Constructor that does not take as input the itemsAvailable
     * @param itemsCart
     */
    public Cart(List<Item> itemsCart) {
        this.itemsCart = itemsCart;
    }
    
    /**
     * @param bookID
     * @param itemsAvail
     * @param books
     * @param cartBooks
     */
    public void searchBook(String bookID, List<Item> itemsAvail, List<Book> books, List<Book> cartBooks){
        for(Item i : itemsAvail){
            if(i instanceof Book){
                    books.add((Book) i);
            }
        }
        for(Book b : books){
            if(b.getBookID().equals(bookID)){
                cartBooks.add(b);
            }
        }
        books.clear();
    }

    /**
     * @param accID
     * @param itemsAvail
     * @param accs
     * @param cartAccs
     */
    public void searchAccs(String accID, List<Item> itemsAvail, List<Accessories> accs, List<Accessories> cartAccs){
        for(Item i : itemsAvail){
            if(i instanceof Accessories){
                    accs.add((Accessories) i);
            }
        }
        for(Accessories a : accs){
            if(a.getAccesoryID().equals(accID)){
                cartAccs.add(a);
            }
        }
        accs.clear();
    }

    /**
     * @param ItemId
     * @return Cart filled with all items (Accessories or Books)
     * @throws Exception
     */
    public Cart addItemToCart(String ItemId) throws Exception {

        boolean itemIsOnStock = false;

        List<Accessories> accs = new ArrayList<Accessories>();
        List<Book> books = new ArrayList<Book>();

        for(Item i : itemsAvailable) {
            if(i instanceof Accessories) {
                accs.add((Accessories) i);
            }
            if(i instanceof Book) {
                books.add((Book) i);
            }
        }

        for(Book bc: books) {
            if(bc.getBookID().equals(ItemId)) {
                itemIsOnStock = true;
                if(this.itemsCart.contains(bc)){
                    bc.setStock(bc.getStock() + 1);
                }else{
                    bc.setStock(1);
                }
                this.itemsCart.add(bc);
            }
        }
        if(itemIsOnStock == false) {
            for(Accessories ac: accs) {
                if(ac.getAccesoryID().equals(ItemId)) {
                    itemIsOnStock = true;
                    if(this.itemsCart.contains(ac)){
                        ac.setStock(ac.getStock() + 1);
                    }else{
                        ac.setStock(1);
                    }
                    this.itemsCart.add(ac);
                }
            }
        }
        if(itemIsOnStock == false) {
            throw new Exception("The item you selected is not currently on stock");
        }
        Cart cart = new Cart(this.itemsCart);
        return cart;
    }

    /**
     * @return List<Book>, filters through the itemsAvailable and returns list of the filtered items
     */
    public List<Book> filterBooks(){
        List<Book> books = new ArrayList<Book>();
        for(int i = 0; i < itemsAvailable.size(); i++){
            if(itemsAvailable.get(i) instanceof Book){
                books.add((Book) itemsAvailable.get(i));
            }
        }
        return books;
    }

    /**
     * @return List<Accessories>, filters through the itemsAvailable and returns list of the filtered items
     */
    public List<Accessories> filterAcc(){
        List<Accessories> accs = new ArrayList<Accessories>();
        for(int i = 0; i < itemsAvailable.size(); i++){
            if(itemsAvailable.get(i) instanceof Accessories){
                accs.add((Accessories) itemsAvailable.get(i));
            }
        }
        return accs;
    }

    /**
     * Prints all items that the user chose, or in other words prints all items that are in the cart
     */
    public void printsCart() {

        List<Item> cart = this.itemsCart;
        List<Item> printedItems = new ArrayList<Item>();
        boolean firstRun = true;

        System.out.println("\n                              ------ Items added to Cart ---------       \n");

        for(Item item : cart) {
            if(!printedItems.contains(item) && firstRun == false){
                System.out.println(item);
                printedItems.add(item);
            }else if(firstRun){
                System.out.println(item);
                printedItems.add(item);
                firstRun = false;
            }
        }
    }

    /**
     *  Prints all items that are available in the bookstore
     */
    public void printItems() {
        int counter  = 1;

        boolean firstRun = true;

        List<Book> printedBooks = new ArrayList<Book>();
        List<Accessories> printedAcc = new ArrayList<Accessories>();

        List<Book> books = filterBooks();
        List<Accessories> accs = filterAcc();

        System.out.println("\n                    --- Books ---           \n");

        for(Book b: books) {
            if(!printedBooks.contains(b) && firstRun == false){
                System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Item ID: " + counter++);
                System.out.println(b);
                printedBooks.add(b);
            }else if(firstRun){
                System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Item ID: " + counter++);
                System.out.println(b);
                printedBooks.add(b);
                firstRun = false;
            }
        }

        System.out.println("\n                  --- Accessories ---           \n");

        for(Accessories a: accs) {
            if(!printedAcc.contains(a) && firstRun == false){
                System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Item ID: " + counter++);
                System.out.println(a);
                printedAcc.add(a);
            }else{
                System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Item ID: " + counter++);
                System.out.println(a);
                printedAcc.add(a);
                firstRun = false;
            }
        }
    }

    /**
     * @return int of the size of the cart
     */
    public int printSize() {
        return this.itemsCart.size();
    }

    /**
     * @return list of the items of the cart
     */
    public List<Item> getCart() {

        return this.itemsCart;
    }

    /**
     * @return List of the Stock, or the items that are available
     */
    public List<Item> getInventory() {

        return this.itemsAvailable;
    }
}
