package bookstore.RewardsType;

public class Coupons{
    private int coupId;
    private String coupName;
    private double discount;

    /**
     * Constructor which sets these params to these fields
     * @param coupId
     * @param coupName
     * @param discount
     */
    public Coupons(int coupId, String coupName, double discount) {
        this.coupId = coupId;
        this.coupName = coupName;
        this.discount = discount;
    }

    /**  
     * Constructor so the sql can work
     **/
    public Coupons() {

    }

    /** 
     * @return coupID
     **/
    public int getCoupId() {
        return coupId;
    }

    /**  
     * @return coupName
     **/
    public String getCoupName() {
        return coupName;
    }

    /**  
     * @return discount
     **/
    public double getDiscount() {
        return discount;
    }

    /**  set coupID
     * @param coupId
     **/
    public void setCoupId(int coupId) {
        this.coupId = coupId;
    }

    /**  set coupName
     * @param coupName
     **/
    public void setCoupName(String coupName) {
        this.coupName = coupName;
    }

    /**  set discount
     * @param discount
     **/
    public void setDiscount(double discount) {
        this.discount = discount;
    }
}