DROP TABLE STORE_ONLINE_ORDERS;
DROP TABLE STORE_ORDERS;
DROP TABLE STORE_PAYMENT_COUPONS;
DROP TABLE STORE_CUSTOMERS;
DROP TABLE STORE_MEMBERSHIPS;
DROP TABLE STORE_PAYMENT_INFO;
DROP TABLE STORE_COUPONS;
DROP TABLE STORE_ACCESSORIES;
DROP TABLE STORE_BOOK_AUTHORS;
DROP TABLE STORE_AUTHORS;
DROP TABLE STORE_BOOKS;
DROP TABLE STORE_PUBLISHERS;

CREATE TABLE STORE_PUBLISHERS (
    pub_ID number(4) PRIMARY KEY,
    pub_name varchar2(75) NOT NULL,
    contact_name varchar2(60) NOT NULL,
    phone_number varchar2(18) NOT NULL 
);

CREATE TABLE STORE_BOOKS (
    book_ID number(5) PRIMARY KEY,
    title varchar2(75) NOT NULL,
    pub_ID number(4) REFERENCES STORE_PUBLISHERS(pub_ID),
    book_cost number(5,2) DEFAULT 0 NOT NULL,
    book_retail number(5,2) DEFAULT 0 NOT NULL,
    genre varchar2(80),
    description varchar2(1000),
    stock number(3,0) DEFAULT 0 NOT NULL,
    type varchar2(10) NOT NULL
);

CREATE TABLE STORE_AUTHORS (
    author_ID number(5) PRIMARY KEY,
    author_name varchar2(60) NOT NULL,
    author_bio varchar2(600)
);

CREATE TABLE STORE_BOOK_AUTHORS (
    author_ID number(5) REFERENCES STORE_AUTHORS(author_ID),
    book_ID number(5) REFERENCES STORE_BOOKS(book_ID),
    author_share number(5,2) NOT NULL
);

CREATE TABLE STORE_ACCESSORIES (
    acc_ID number(5) PRIMARY KEY,
    acc_name varchar2(60) NOT NULL,
    acc_cost number(6,2) DEFAULT 0 NOT NULL,
    acc_retail number(7,2) DEFAULT 0 NOT NULL,
    acc_description varchar2(600),
    acc_stock number(5,0) DEFAULT 0 NOT NULL
);

CREATE TABLE STORE_COUPONS (
    coup_ID number(3) PRIMARY KEY,
    coup_name varchar2(60) NOT NULL,
    discount number(5,2) NOT NULL
);

CREATE TABLE STORE_PAYMENT_INFO (
    pay_ID number(5) PRIMARY KEY,
    card_num number(16) NOT NULL,
    balance number(8,2) DEFAULT 0 NOT NULL,
    points number (10,2) DEFAULT 0 NOT NULL
);

CREATE TABLE STORE_MEMBERSHIPS (
    mem_type number(1) PRIMARY KEY,
    medal varchar2(16) NOT NULL
);

CREATE TABLE STORE_CUSTOMERS (
    cust_ID number(6) PRIMARY KEY,
    cust_name varchar2(60) NOT NULL,
    mem_type number(1) REFERENCES STORE_MEMBERSHIPS(mem_type),
    pay_ID number(5) REFERENCES STORE_PAYMENT_INFO(pay_ID),
    cust_phone varchar2(18),
    cust_email varchar2(40) NOT NULL
);

CREATE TABLE STORE_PAYMENT_COUPONS (
    coup_ID number(3) REFERENCES STORE_COUPONS(coup_ID),
    cust_ID number(6) REFERENCES STORE_CUSTOMERS(cust_ID)
);

CREATE TABLE STORE_ORDERS (
    order# number(6) PRIMARY KEY,
    cust_ID number(6) REFERENCES STORE_CUSTOMERS(cust_ID),
    order_date date NOT NULL,
    ship_date date,
    address varchar2(120) NOT NULL
);

CREATE TABLE STORE_ONLINE_ORDERS (
    o_order# number(6) PRIMARY KEY,
    cust_ID number(6) REFERENCES STORE_CUSTOMERS(cust_ID),
    order_date date NOT NULL
);

CREATE OR REPLACE VIEW account_details AS
SELECT C.CUST_ID, C.CUST_NAME, M.MEDAL, C.CUST_PHONE, C.CUST_EMAIL, P.CARD_NUM, P.BALANCE, P.POINTS FROM STORE_CUSTOMERS C
INNER JOIN STORE_MEMBERSHIPS M ON M.MEM_TYPE = C.MEM_TYPE
INNER JOIN STORE_PAYMENT_INFO P ON P.PAY_ID = C.PAY_ID;

CREATE OR REPLACE PROCEDURE update_balance(selected_customer_id STORE_CUSTOMERS.cust_id%TYPE, new_balance STORE_PAYMENT_INFO.balance%TYPE) AS
    payment_id STORE_CUSTOMERS.pay_id%TYPE;
BEGIN
    SELECT pay_id INTO payment_id FROM STORE_CUSTOMERS sc
    WHERE selected_customer_id = sc.cust_id;
    
    UPDATE STORE_PAYMENT_INFO SET balance = new_balance
    WHERE STORE_PAYMENT_INFO.pay_id = payment_id;
END;
/

CREATE OR REPLACE PROCEDURE update_points(selected_customer_id STORE_CUSTOMERS.cust_id%TYPE, added_points STORE_PAYMENT_INFO.points%TYPE) AS
    payment_id STORE_CUSTOMERS.pay_id%TYPE;
BEGIN
    SELECT pay_id INTO payment_id FROM STORE_CUSTOMERS sc
    WHERE selected_customer_id = sc.cust_id;
    
    UPDATE STORE_PAYMENT_INFO SET points = added_points
    WHERE STORE_PAYMENT_INFO.pay_id = payment_id;
END;
/


INSERT ALL

INTO STORE_PUBLISHERS VALUES (1, 'Montreal Publishing', 'Alex Lamontagne', '438-276-4653')

INTO STORE_PUBLISHERS VALUES (2, 'Snowy Peaks Publishing', 'Veritas Vasileiou','800-659-3154')

INTO STORE_PUBLISHERS VALUES (3, 'Bookworm Publishing', 'Natalia Sarkissian', '450-167-9123')

INTO STORE_PUBLISHERS VALUES (4, 'Flatlands Publishing', 'Michelle Rodriguez', '438-670-1435')

INTO STORE_BOOKS VALUES (1, 'Fitness and Health: Debunking Myths', 1, 45, 70, 'Health, Fitness', 
'Did you know that a lot of the things you hear about health are usually fake and mostly told to make profits off of you? Buy this book to find out all of the lies you have been told!', 10, 'Online')

INTO STORE_BOOKS VALUES (2, 'A Deep Dive into E-Sports; Why are Losers Making Money?', 2, 70, 100, 'Gaming', 
'While gamers and nerds alike were bullied back in the day, they are viewed as celebritites today. How does this affect our society and how should we punish these people?', 10, 'Physical')

INTO STORE_BOOKS VALUES (3, 'How Do You Talk With Humans?', 3, 90, 140, 'Guides, Self-Help, Research, Aliens, Biology, Social Dilemmas', 
'What are humans? Why do we want to talk to them? What is socializing? Why do we feel love? Finally, how do you talk? We researched these questions extensively and published our findings in this book', 10, 'Physical')

INTO STORE_BOOKS VALUES (4, 'What IS Wrong with you?', 4, 5, 20, 'Guides, Self-Help, Research', 'You have a problem... Let us fix that.', 10, 'Online')

INTO STORE_AUTHORS VALUES (1, 'Auron Limelick', 'A bodybuilder at day, writer at night. This man is eager to help people be their best selves and will often write about health.')

INTO STORE_AUTHORS VALUES (2, 'Chad Brosky', 'An alpha male who never backs down from a challenge. Is not afraid to bully if necessary.')

INTO STORE_AUTHORS VALUES (3, 'Amelie Newton', 'A mysterious woman who never talks in interviews and refuses to write a bio. All we know is that her books are bangers!')

INTO STORE_AUTHORS VALUES (4, 'Vasil Brokeback', 'An aggressive, inquisitive and blunt person who is genuinely interested in helping you')

INTO STORE_BOOK_AUTHORS VALUES (1, 1, 45)

INTO STORE_BOOK_AUTHORS VALUES (2, 2, 70)

INTO STORE_BOOK_AUTHORS VALUES (3, 3, 55)

INTO STORE_BOOK_AUTHORS VALUES (4, 4, 35)

INTO STORE_ACCESSORIES VALUES (5, 'Book Light', 15, 35, 'A powerful book light to help you read in the dark.', 10)

INTO STORE_ACCESSORIES VALUES (6, 'Library Scented Candle', 25, 40, 'Remind yourself of the pleasant smell of books at home', 10)

INTO STORE_ACCESSORIES VALUES (7,'Mug',10,12,'Mug with the main character on it',10)

INTO STORE_ACCESSORIES VALUES (8,'T-shirt',15,17,'T-shirt with the main character on it',10)

INTO STORE_ACCESSORIES VALUES (9,'Hoodie',20,22,'Hoodies with the emblem of the book',10)

INTO STORE_ACCESSORIES VALUES (10,'Sticker',5,7,'Sticker with the main character on it',10)

INTO STORE_ACCESSORIES VALUES (11,'Action Figure',30,32,'Figurine of the main character',10)

INTO STORE_ACCESSORIES VALUES (12,'Pen',3,5,'Pen with the emblem of the hero on it',10)

INTO STORE_COUPONS VALUES (1, 'Bronze', 5)

INTO STORE_COUPONS VALUES (2, 'Silver', 7.5)

INTO STORE_COUPONS VALUES (3, 'Gold', 12.5)

INTO STORE_PAYMENT_INFO VALUES (1, '1001675819025454', 10, 70)

INTO STORE_PAYMENT_INFO VALUES (2, '5467102467583254', 0, 0)

INTO STORE_PAYMENT_INFO VALUES (3, '0120676593241023', 40, 90)

INTO STORE_PAYMENT_INFO VALUES (4, '4828743512938153', 30, 30)

INTO STORE_MEMBERSHIPS VALUES (1, 'Base')

INTO STORE_MEMBERSHIPS VALUES (2, 'Bronze')

INTO STORE_MEMBERSHIPS VALUES (3, 'Silver')

INTO STORE_MEMBERSHIPS VALUES (4, 'Gold')

INTO STORE_CUSTOMERS VALUES (1, 'Ricardo Magneloz', 1, 1, '4387651022', 'rMagneloz@gmail.com')

INTO STORE_CUSTOMERS VALUES (2, 'Vikki Flower', 2, 2, '4503245789', 'sneakyKitty@hotmail.com')

INTO STORE_CUSTOMERS VALUES (3, 'Nathan Granger', 3, 3, '8009115403', 'nathanBlows@gmail.com')

INTO STORE_CUSTOMERS VALUES (4, 'Hans Cole', 4, 4, '4387940341', 'supremeJedi@gmail.com')

SELECT * FROM DUAL;
